package org.food;

public final class Food {
    private String brandName;
    private String description;
    private int calories;
    private double servingsPerContainer;
    private int servingSize;
    private String servingMetricUnit;

    public Food() { }

    public Food(String brandName, String description, int calories,
                double servingsPerContainer,
                int servingSize, String servingMetricUnit) {
        this.brandName = brandName;
        this.description = description;
        this.calories = calories;
        this.servingsPerContainer = servingsPerContainer;
        this.servingSize = servingSize;
        this.servingMetricUnit = servingMetricUnit;
    }

    public double getServingsPerContainer() {
        return servingsPerContainer;
    }

    public void setServingsPerContainer(double servingsPerContainer) {
        this.servingsPerContainer = servingsPerContainer;
    }

    public int getServingSize() {
        return servingSize;
    }

    public void setServingSize(int servingSize) {
        this.servingSize = servingSize;
    }

    public String getServingMetricUnit() {
        return servingMetricUnit;
    }

    public void setServingMetricUnit(String servingMetricUnit) {
        this.servingMetricUnit = servingMetricUnit;
    }


    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCalories() {
        return this.calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }


}
