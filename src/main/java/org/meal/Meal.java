package org.meal;

import org.food.Food;

import java.util.Map;
import java.util.HashMap;

public final class Meal {
    private Map<Food, Double> foods;
    public Meal() {
        foods = new HashMap<>();
    }

    public Meal(Map<Food, Double> foods) {
        setFoods(foods);
    }
    public Map<Food, Double> getFoods() {
        return foods;
    }

    public void setFoods(Map<Food, Double> foods) {
        this.foods = new HashMap<>(foods);
    }

    public void addFood(Food food, Double quantity) {
        if (quantity == 0) {
            return;
        }

        Double count = foods.getOrDefault(food, 0.0) + quantity;
        foods.put(food, count);
    }

    public void removeFood(Food food) {
        foods.remove(food);
    }
}
