package org.exercise.strength;
import org.exercise.Exercise;

public final class StrengthExercise extends Exercise {
    private int numberOfSets;
    private int repetitions;
    private int weightPerSet;

    public StrengthExercise(int caloriesBurned, String desc, int numberOfSets,
                            int repetitions, int weightPerSet) {
        super(caloriesBurned, desc);
        this.setNumberOfSets(numberOfSets);
        this.setRepetitions(repetitions);
        this.setWeightPerSet(weightPerSet);
    }

    public StrengthExercise(StrengthExercise other) {
        super(other);
        this.numberOfSets = other.numberOfSets;
        this.repetitions = other.repetitions;
        this.weightPerSet = other.weightPerSet;
    }

    public int getNumberOfSets() {
        return numberOfSets;
    }

    public void setNumberOfSets(int numberOfSets) {
        if (numberOfSets <= 0) {
            throw new IllegalArgumentException("Value must be non-negative");
        }
        this.numberOfSets = numberOfSets;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        if (repetitions <= 0) {
            throw new IllegalArgumentException("Value must be non-negative");
        }
        this.repetitions = repetitions;
    }

    public int getWeightPerSet() {
        return weightPerSet;
    }

    public void setWeightPerSet(int weightPerSet) {
        if (weightPerSet <= 0) {
            throw new IllegalArgumentException("Value must be non-negative");
        }
        this.weightPerSet = weightPerSet;
    }
}
