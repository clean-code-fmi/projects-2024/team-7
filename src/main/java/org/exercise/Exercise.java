package org.exercise;

public abstract class Exercise {
    private int caloriesBurned;
    private String description;

    public Exercise(int caloriesBurned, String description) {
        this.setCaloriesBurned(caloriesBurned);
        this.setDescription(description);
    }

    public Exercise(Exercise other) {
        this.caloriesBurned = other.caloriesBurned;
        this.description = other.description;
    }

    public final String getDescription() {
        return description;
    }

    public final void setDescription(String description) {
        if (description.isEmpty()) {
            throw new IllegalArgumentException("Value must be non-negative");
        }
        this.description = description;
    }

    public final int getCaloriesBurned() {
        return caloriesBurned;
    }

    public final void setCaloriesBurned(int caloriesBurned) {
        if (caloriesBurned <= 0) {
            throw new IllegalArgumentException("Value must be non-negative");
        }
        this.caloriesBurned = caloriesBurned;
    }
}
