package org.exercise.cardio;
import org.exercise.Exercise;

public final class CardioExercise extends Exercise {
    private int durationMinutes;

    public CardioExercise(int caloriesBurned, int durationMinutes,
                          String desc) {
        super(caloriesBurned, desc);
        this.setDurationMinutes(durationMinutes);
    }

    public CardioExercise(CardioExercise other) {
        super(other);
        this.durationMinutes = other.durationMinutes;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        if (durationMinutes <= 0) {
            throw new IllegalArgumentException("Value must be non-negative");
        }
        this.durationMinutes = durationMinutes;
    }
}
