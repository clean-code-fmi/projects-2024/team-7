package org.day;

import org.enums.MealTimes;

public final class Day {
    private double breakfastCalories;
    private double lunchCalories;
    private double dinnerCalories;
    private double snacksCalories;
    private  double waterIntake;
    private double caloriesBurned;

    public void setCalories(MealTimes mealTime, double caloriesBurned) {
        if (caloriesBurned < 0) {
            return;
        }
        switch (mealTime) {
            case Breakfast:
                breakfastCalories += caloriesBurned;
            case Lunch:
                lunchCalories += caloriesBurned;
            case Dinner:
                dinnerCalories += caloriesBurned;
            default:
                snacksCalories += caloriesBurned;
        }
    }
    public void setCaloriesBurned(double caloriesBurned) {
        if (caloriesBurned > 0) {
            this.caloriesBurned += caloriesBurned;
        }
    }

    public void setWaterIntake(double waterIntake) {
        if (waterIntake > 0) {
            this.waterIntake += waterIntake;
        }
    }

    public double getWaterIntake() {
        return waterIntake;
    }
    public double getBreakfastCalories() {
        return breakfastCalories;
    }
    public double getLunchCalories() {
        return lunchCalories;
    }
    public double getDinnerCalories() {
        return dinnerCalories;
    }
    public double getSnacksCalories() {
        return snacksCalories;
    }
    public double getCaloriesBurned() {
        return caloriesBurned;
    }
}
