package org.account.user;

import org.enums.ActivityLevel;
import org.enums.MuscleGoalType;
import org.enums.WeightGoalType;
import org.account.goals.Goal;
import org.account.goals.MuscleGoal;
import org.account.goals.WeightGoal;

import java.security.InvalidParameterException;
import java.util.List;

public final class User {
    private List<Goal> goals;
    private ActivityLevel activityLevel;
    private char gender;
    private int height;
    private String firstName;
    private String lastName;

    public ActivityLevel getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(String activityLevelInput) {
        // parse it
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char genderInput) {
        if (genderInput != 'm' && genderInput != 'f') {
            throw new InvalidParameterException("Please select either m or f.");
        }

        this.gender = genderInput;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstNameInput) {
        int length = firstNameInput.length();

        if (length < 3 || length > 20) {
            String errorMessage = "First name must be between 3 and 20 symbols";
            throw new InvalidParameterException(errorMessage);
        }

        this.firstName = firstNameInput;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastNameInput) {
        int length = lastNameInput.length();

        if (length < 3 || length > 20) {
            String errorMessage = "Last name must be between 3 and 20 symbols";
            throw new InvalidParameterException(errorMessage);
        }

        this.lastName = lastName;
    }

    public void setGoals(List<String> goalsInput) {
        int numberOfGoals = goalsInput.toArray().length;

        if (numberOfGoals > 3) {
            throw new InvalidParameterException("Goals must be maximum 3.");
        }

        for (int i = 0; i < numberOfGoals; i++) {
            String currentGoal = goalsInput.get(i);

            MuscleGoalType muscleGoal = MuscleGoalType.valueOf(currentGoal);
            this.goals.add(new MuscleGoal(muscleGoal));

            WeightGoalType weightGoal = WeightGoalType.valueOf(currentGoal);
            this.goals.add(new WeightGoal(weightGoal));
        }
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int heightInput) {
        if (heightInput <= 0) {
            throw new InvalidParameterException("Height must be above 0.");
        }

        this.height = heightInput;
    }
}
