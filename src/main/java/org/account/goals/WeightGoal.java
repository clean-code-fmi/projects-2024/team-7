package org.account.goals;

import org.enums.WeightGoalType;

public class WeightGoal extends Goal {
    private WeightGoalType weightGoal;

    public WeightGoal() {
        weightGoal = null;
    }

    public WeightGoal(WeightGoalType weightGoalInput) {
        weightGoal = weightGoalInput;
    }
}
