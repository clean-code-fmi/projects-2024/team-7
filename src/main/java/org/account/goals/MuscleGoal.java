package org.account.goals;

import org.enums.MuscleGoalType;

public class MuscleGoal extends Goal {
    private MuscleGoalType muscleGoal;

    public MuscleGoal() {
        muscleGoal = null;
    }

    public MuscleGoal(MuscleGoalType muscleGoalInput) {
        muscleGoal = muscleGoalInput;
    }
}
