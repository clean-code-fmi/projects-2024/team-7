package org.loggers.mealLogger;

import org.day.Day;
import org.loggers.ILogger;
import org.loggers.configurable.Configurable;
import org.meal.Meal;

public final class MealLogger implements ILogger {
    /* when logging a meal we would also take a parameter
       with the meal time (same as when logging food)
       We are working under the assumption that in each meal
       there is only one portion of each food*/
    @Override
    public void log(Day day, Configurable config) {
        final double[] totalCalories = {0};
        Meal meal = new Meal(config.getFoods());
        meal.getFoods()
                .forEach((food, calories) -> {
                    totalCalories[0] += calories;
                });

        day.setCalories(config.getMealTime(), config.getCaloriesBurned());
    }
}
