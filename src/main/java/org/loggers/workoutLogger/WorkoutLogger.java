package org.loggers.workoutLogger;

import org.day.Day;
import org.exercise.Exercise;
import org.loggers.ILogger;
import org.loggers.configurable.Configurable;
import org.workout.Workout;

import java.util.Set;

public final class WorkoutLogger implements ILogger {
    @Override
    public void log(Day day, Configurable configurable) {
        Workout workout = new Workout(configurable.getExercises());
        Set<Exercise> exercises = workout.getExercises();

        for (Exercise exercise: exercises) {
            day.setCaloriesBurned(exercise.getCaloriesBurned());
        }
    }
}
