package org.loggers.configurable;

import org.enums.MealTimes;
import org.exercise.Exercise;
import org.food.Food;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class Configurable {
    // Food
    private String brandName;
    private String foodDescription;
    private int calories;
    private double servingsPerContainer;
    private int servingSize;
    private String servingMetricUnit;
    private double numberOfServings;

    // Exercise
    private int caloriesBurned;
    private String exerciseDescription;
    private int numberOfSets;
    private int repetitions;
    private int weightPerSet;
    private int durationMinutes;

    // Meal
    private Map<Food, Double> foods = new HashMap<Food, Double>();

    // Workout
    private Set<Exercise> exercises = new HashSet<Exercise>();

    private MealTimes mealTime;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getFoodDescription() {
        return foodDescription;
    }

    public void setFoodDescription(String foodDescription) {
        this.foodDescription = foodDescription;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public double getServingsPerContainer() {
        return servingsPerContainer;
    }

    public void setServingsPerContainer(double servingsPerContainer) {
        this.servingsPerContainer = servingsPerContainer;
    }

    public int getServingSize() {
        return servingSize;
    }

    public void setServingSize(int servingSize) {
        this.servingSize = servingSize;
    }

    public String getServingMetricUnit() {
        return servingMetricUnit;
    }

    public void setServingMetricUnit(String servingMetricUnit) {
        this.servingMetricUnit = servingMetricUnit;
    }

    public int getCaloriesBurned() {
        return caloriesBurned;
    }

    public void setCaloriesBurned(int caloriesBurned) {
        this.caloriesBurned = caloriesBurned;
    }

    public String getExerciseDescription() {
        return exerciseDescription;
    }

    public void setExerciseDescription(String exerciseDescription) {
        this.exerciseDescription = exerciseDescription;
    }

    public int getNumberOfSets() {
        return numberOfSets;
    }

    public void setNumberOfSets(int numberOfSets) {
        this.numberOfSets = numberOfSets;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getWeightPerSet() {
        return weightPerSet;
    }

    public void setWeightPerSet(int weightPerSet) {
        this.weightPerSet = weightPerSet;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public Map<Food, Double> getFoods() {
        return foods;
    }

    public void setFoods(Map<Food, Double> foods) {
        this.foods = foods;
    }

    public Set<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(Set<Exercise> exercises) {
        this.exercises = exercises;
    }

    public MealTimes getMealTime() {
        return mealTime;
    }

    public void setMealTime(MealTimes mealTime) {
        this.mealTime = mealTime;
    }

    public double getNumberOfServings() {
        return numberOfServings;
    }

    public void setNumberOfServings(double numberOfServings) {
        this.numberOfServings = numberOfServings;
    }
}
