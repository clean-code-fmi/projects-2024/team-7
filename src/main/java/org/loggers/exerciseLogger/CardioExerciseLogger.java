package org.loggers.exerciseLogger;

import org.day.Day;
import org.exercise.cardio.CardioExercise;
import org.loggers.ILogger;
import org.loggers.configurable.Configurable;

public final class CardioExerciseLogger implements ILogger {
    @Override
    public void log(Day day, Configurable config) {
        CardioExercise ex = new CardioExercise(config.getCaloriesBurned(),
                config.getDurationMinutes(), config.getExerciseDescription());

        double caloriesBurned = ex.getCaloriesBurned();
        day.setCaloriesBurned(caloriesBurned);
    }
}
