package org.loggers.exerciseLogger;

import org.day.Day;
import org.exercise.strength.StrengthExercise;
import org.loggers.ILogger;
import org.loggers.configurable.Configurable;

public final class StrengthExerciseLogger implements ILogger {

    @Override
    public void log(Day day, Configurable config) {
        StrengthExercise ex = new StrengthExercise(config.getCaloriesBurned(),
                config.getExerciseDescription(), config.getNumberOfSets(),
                config.getRepetitions(), config.getWeightPerSet());

        double caloriesBurned = ex.getCaloriesBurned();
        day.setCaloriesBurned(caloriesBurned);
    }
}
