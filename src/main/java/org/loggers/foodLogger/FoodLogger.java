package org.loggers.foodLogger;

import org.day.Day;
import org.loggers.configurable.Configurable;
import org.loggers.ILogger;

public final class FoodLogger implements ILogger {
    @Override
    public void log(Day day, Configurable config) {
        double caloriesIntake = config.getCalories()
                * config.getNumberOfServings();

        day.setCalories(config.getMealTime(), config.getCaloriesBurned());
    }
}
