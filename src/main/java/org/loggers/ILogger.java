package org.loggers;

import org.day.Day;
import org.loggers.configurable.Configurable;

public interface ILogger {
    void log(Day day, Configurable configurable);
}
