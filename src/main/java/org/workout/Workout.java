package org.workout;

import org.exercise.Exercise;

import java.util.HashSet;
import java.util.Set;

public final class Workout {
    private Set<Exercise> exercises;

    public Workout() {
        this.exercises = new HashSet<>();
    }

    public Workout(Set<Exercise> exercises) {
        setExercises(exercises);
    }

    public void setExercises(Set<Exercise> exercises) {
        this.exercises = new HashSet<>(exercises);
    }

    public Set<Exercise> getExercises() {
        return exercises;
    }

    public void addExercise(Exercise exercise) {
        this.exercises.add(exercise);
    }

    public void removeExercise(Exercise exercise) {
        this.exercises.remove(exercise);
    }
}
