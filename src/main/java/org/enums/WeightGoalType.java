package org.enums;

public enum WeightGoalType {
    LoseWeight,
    MaintainWeight,
    GainWeight
}
