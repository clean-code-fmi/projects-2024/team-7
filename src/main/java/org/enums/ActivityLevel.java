package org.enums;

public enum ActivityLevel {
    NotVeryActive,
    LightlyActive,
    Active,
    VeryActive
}
