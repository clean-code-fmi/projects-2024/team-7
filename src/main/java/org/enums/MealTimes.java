package org.enums;

public enum MealTimes {
    Breakfast,
    Lunch,
    Dinner,
    Snack
}
