package org.enums;

public enum MuscleGoalType {
    GainMuscle,
    MaintainMuscle
}
