package org.food;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public final class FoodTest {

    @Test
    void testCal() {
        int expectedCal = 250;
        Food f = new Food("Lind", "Lindor", expectedCal, 12, 25, "grams");
        assertEquals(expectedCal, f.getCalories());
    }

    void testBrandName() {
        String expectedBrandName = "Lind";
        Food f = new Food(expectedBrandName, "Lindor", 250, 12, 25, "grams");
        assertEquals(expectedBrandName, f.getBrandName());
    }

    void testDesc() {
        String expectedDesc = "Lindor";
        Food f = new Food("Lind", expectedDesc, 250, 12, 25, "grams");
        assertEquals(expectedDesc, f.getDescription());
    }

    void testServingsPerContainer() {
        int expectedServings = 12;
        Food f = new Food("Lind", "Lindor", 250, expectedServings, 25, "grams");
        assertEquals(expectedServings, f.getServingsPerContainer());
    }

    void testServingSize() {
        int expectedServingSize = 25;
        Food f = new Food("Lind", "Lindor", 250, 12,
                            expectedServingSize, "grams");
        assertEquals(expectedServingSize, f.getServingSize());
    }

    void testServingMetricUnit() {
        String expectedMetricUnit = "grams";
        Food f = new Food("Lind", "Lindor", 250, 12, 25, expectedMetricUnit);
        assertEquals(expectedMetricUnit, f.getServingMetricUnit());
    }
}
