package org.exercise;

import org.exercise.strength.StrengthExercise;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StrengthExerciseTest {
    @Test
    void testCal() {
        int expectedCal = 100;
        StrengthExercise strengthTest = new StrengthExercise(expectedCal,
                "squats", 5, 12, 50);
        assertEquals(expectedCal, strengthTest.getCaloriesBurned());
    }

    @Test
    void testNumberOfSets() {
        int expectedNumberOfSets = 5;
        StrengthExercise strengthTest = new StrengthExercise(100, "squats",
                expectedNumberOfSets, 12, 50);
        assertEquals(expectedNumberOfSets, strengthTest.getNumberOfSets());
    }

    @Test
    void testRepetitions() {
        int expectedRepetitions = 12;
        StrengthExercise strengthTest = new StrengthExercise(100, "squats",
                5, expectedRepetitions, 50);
        assertEquals(expectedRepetitions, strengthTest.getRepetitions());
    }

    @Test
    void testWeightPerSet() {
        int expectedWeightPerSet = 50;
        StrengthExercise strengthTest = new StrengthExercise(100, "squats",
                5, 12, expectedWeightPerSet);
        assertEquals(expectedWeightPerSet, strengthTest.getWeightPerSet());
    }

    @Test
    void testDesc() {
        String expectedDesc = "squats";
        StrengthExercise strengthTest = new StrengthExercise(100, expectedDesc,
                5, 12, 50);
        assertEquals(expectedDesc, strengthTest.getDescription());
    }
}
