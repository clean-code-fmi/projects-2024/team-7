package org.exercise;

import org.exercise.cardio.CardioExercise;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CardioExerciseTest {

    @Test
    void testCal() {
        int expectedCal = 52;
        CardioExercise cardioTest = new CardioExercise(expectedCal, 120,
                                                        "running");
        assertEquals(expectedCal, cardioTest.getCaloriesBurned());
    }

    @Test
    void testMin() {
        int expectedMin = 120;
        CardioExercise cardioTest = new CardioExercise(52, expectedMin,
                                                        "running");
        assertEquals(expectedMin, cardioTest.getDurationMinutes());
    }

    @Test
    void testDesc() {
        String expectedDesc = "running";
        CardioExercise cardioTest = new CardioExercise(52, 120, expectedDesc);
        assertEquals(expectedDesc, cardioTest.getDescription());
    }
}
