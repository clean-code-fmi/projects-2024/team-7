package org.workout;

import org.exercise.Exercise;
import org.exercise.cardio.CardioExercise;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class WorkoutTest {
    @Test
    void testAddWorkout() {
        Exercise ex = new CardioExercise(1, 1, "a");
        Workout workout = new Workout();

        workout.addExercise(ex);
        assertEquals(1, workout.getExercises().size());
    }

    @Test
    void addExistingFood() {
        Exercise ex = new CardioExercise(1, 1, "a");
        Workout workout = new Workout();

        workout.addExercise(ex);
        workout.addExercise(ex);

        assertEquals(1, workout.getExercises().size());
    }

    @Test
    void testRemoveFood() {
        Exercise ex = new CardioExercise(1, 1, "a");
        Workout workout = new Workout();

        workout.addExercise(ex);
        workout.removeExercise(ex);

        assertTrue(workout.getExercises().isEmpty());
    }
}
