package org.day;

import org.enums.MealTimes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class DayTest {

    @Test
    void testSetBreakfastCalories() {
        double testCalories = 12;
        Day day = new Day();
        day.setCalories(MealTimes.Breakfast, 12);
        assertEquals(testCalories, day.getBreakfastCalories());
    }

    @Test
    void testSetBreakfastCaloriesNegative() {
        double testCalories = -1.5;
        Day day = new Day();
        day.setCalories(MealTimes.Breakfast, testCalories);
        double expectedResult = 0;
        assertEquals(expectedResult, day.getBreakfastCalories());
    }

    @Test
    void testSetLunchCalories() {
        double testCalories = 12;
        Day day = new Day();
        day.setCalories(MealTimes.Lunch, 12);
        assertEquals(testCalories, day.getLunchCalories());
    }

    @Test
    void testSetLunchCaloriesNegative() {
        double testCalories = -1.5;
        Day day = new Day();
        day.setCalories(MealTimes.Lunch, testCalories);
        double expectedResult = 0;
        assertEquals(expectedResult, day.getLunchCalories());
    }

    @Test
    void testSetDinnerCalories() {
        double testCalories = 12;
        Day day = new Day();
        day.setCalories(MealTimes.Dinner, 12);
        assertEquals(testCalories, day.getDinnerCalories());
    }

    @Test
    void testSetDinnerCaloriesNegative() {
        double testCalories = -1.5;
        Day day = new Day();
        day.setCalories(MealTimes.Dinner, testCalories);
        double expectedResult = 0;
        assertEquals(expectedResult, day.getDinnerCalories());
    }

    @Test
    void testSetSnacksCalories() {
        double testCalories = 12;
        Day day = new Day();
        day.setCalories(MealTimes.Snack, 12);
        assertEquals(testCalories, day.getSnacksCalories());
    }

    @Test
    void testSetSnacksCaloriesNegative() {
        double testCalories = -1.5;
        Day day = new Day();
        day.setCalories(MealTimes.Snack, testCalories);
        double expectedResult = 0;
        assertEquals(expectedResult, day.getSnacksCalories());
    }

    @Test
    void testSetWaterIntake() {
        double testIntake = 1.3;
        Day day = new Day();
        day.setWaterIntake(1.3);
        assertEquals(testIntake, day.getWaterIntake());
    }

    @Test
    void testSetWaterIntakeNegative() {
        double testCalories = -0.5;
        Day day = new Day();
        day.setWaterIntake(testCalories);
        double expectedResult = 0;
        assertEquals(expectedResult, day.getWaterIntake());
    }

    @Test
    void testSetCaloriesBurned() {
        double testCalories = 250;
        Day day = new Day();
        day.setCaloriesBurned(250);
        assertEquals(testCalories, day.getCaloriesBurned());
    }

    @Test
    void testSetCaloriesBurnedNegative() {
        double testCalories = -40;
        Day day = new Day();
        day.setCaloriesBurned(testCalories);
        double expectedResult = 0;
        assertEquals(expectedResult, day.getCaloriesBurned());
    }
}
