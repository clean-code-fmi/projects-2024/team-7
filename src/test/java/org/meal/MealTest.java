package org.meal;

import org.food.Food;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MealTest {
    @Test
    void testAddFood() {
        Meal meal = new Meal();
        Food food = new Food();

        meal.addFood(food, 1.0);
        assertTrue(meal.getFoods().containsKey(food));
    }

    @Test
    void addExistingFood() {
        Meal meal = new Meal();
        Food food = new Food();

        meal.addFood(food, 1.0);
        meal.addFood(food, 2.0);

        assertEquals(meal.getFoods().size(), 1);
    }

    @Test
    void addZeroQuantity() {
        Meal meal = new Meal();
        Food food = new Food();

        meal.addFood(food, 0.0);
        assertTrue(meal.getFoods().isEmpty());
    }


    @Test
    void testRemoveFood() {
        Meal meal = new Meal();
        Food food = new Food();
        meal.addFood(food, 1.0);
        meal.removeFood(food);
        assertTrue(meal.getFoods().isEmpty());
    }
}
